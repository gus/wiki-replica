[[_TOC_]]

Tor Project is using [NextCloud](https://nc.torproject.net/) as a tool for managing and sharing resources ^[#fn1 1]^ and for collaborative editing ^[#fn2 2]^.

Questions and bug reports are handled by Tor's NextCloud service admin team. For bug reports, please [/newticket create a ticket] in the `Service - nextcloud` component in [Trac](https://trac.torproject.org/). For questions, find us on IRC (GeKo, ln5, pospeselr, anarcat, gaba) or send email to `nextcloud-admin@torproject.org`.

# Tutorial

## Signing in and setting up two-factor authentication
 1. Find an email sent to your personal Tor Project email address from `nc@riseup.net` with a link to `https://nc.torproject.net/`
 1. Do not click on the link in the email, clicking on links in emails is dangerous! Instead, use the safe way: copy and paste the link in the email into your web browser.
 1. Follow the instructions for changing your passphrase.
 1. Enable [two-factor authentication](https://en.wikipedia.org/wiki/Two-factor_authentication) (2FA):
   1. Pick either a [TOTP](https://en.wikipedia.org/wiki/Time-based_One-time_Password_algorithm) or [U2F device](https://en.wikipedia.org/wiki/Universal_2nd_Factor) as an "second factor". TOTP is often done with an app like [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) or a free alternative (for example [free OTP plus](https://github.com/helloworld1/FreeOTPPlus/), see also this [list from the Nextcloud project](https://github.com/nextcloud/twofactor_totp#readme)). U2F is usually supported by security tokens like the [YubiKey](https://en.wikipedia.org/wiki/YubiKey), [Nitrokey](https://www.nitrokey.com/), or similar.
   1. If you have a TOTP setup, locate it and then:
     1. Click "Enable TOTP" on the web page.
     1. Insert your token or start the TOTP application on your handheld device and scan the QR code displayed on the web page.
     1. Enter the numbers from the token/application into the text field on the web page.
     1. Log out and log in again, to verify that you got two factor authentication working.
   1. If you have a U2F setup, locate it and then:
     1. Click the "Add U2F device" button under the "U2F device" section
     1. Insert the token and press the button when prompted by your web browser
     1. Enter a name for the device and click "Add"
     1. Log out and log in again, to verify that you got two factor authentication working.
   1. In NextCloud, select Settings -> Security. The link to your settings can be found by clicking on your "user icon" in the top right corner. Direct link: [Settings -> Security](https://nc.torproject.net/settings/user/security).
   1. Click "Generate Backup codes" in the Two-Factor Authentication section of that page.
   1. Save your backup codes to a password manager of your choice. These will be needed to regain access to your NextCloud account if you ever lose your 2FA token/application.

## A note on credentials
**_Don't let other people use your credentials.**_ Not even people you know and like. If you know someone who should have a NextCloud account, let the service admins know in [/newticket a ticket].

**_Don't let other _other_ people use your credentials.**_ Never enter your passphrase or two-factor code on any other site than Tor Project's NextCloud site. Lower the risk of entering your credentials to the wrong site by verifying that there's a green padlock next to the URL and that the URL is indeed correct.

**_Don't lose your credentials.**_ This is especially important since files are _encrypted_ in a key derived from your passphrase. To help deal with when a phone or hardware token is lost, you should really (really!) generate **Backup codes** and store those in a safe place, together with your passphrase. Backup codes can be used to restore access to your NextCloud and encrypted files. There is no other way of accessing encrypted files! Backup codes can be generated from the [Settings -> Security](https://nc.torproject.net/settings/user/security) page.

## Files
In the top left of the header-bar, you should see a "Folder" icon; when moused over a text label should appear beneath it that says Files. When clicked, you will be taken to the **Files** app and placed in the root of your NextCloud file directory. Here, you can upload local files to NextCloud, download remote files to your local storage, and share remote files across the internet. You can also perform the various file management operations (move, rename, copy, etc) you are familiar with in Explorer on Windows or Finder on macOS.

On the left side of the **Files** app there is a side-bar with a few helpful views of your files.

 * All files : takes you to your root folder
 * Recent : recently accessed files and folders
 * Favorites : bookmarked files and folders
 * Shares : files and folders that have been shared with you or you are sharing with others
 * Tags : search for files and folders by tag

### Upload a file
Local files saved on your computer can be uploaded to NextCloud. To upload a file:

 1. In the NextCloud **Files** app, navigate to the folder where you want to store the file
 1. Click on the cicular button with a **+** inside it (to the right of the little house icon)
 1. Click **Upload file** entry in the context menu
 1. Select a file to upload using your system's file browser window

### Share a file or directory with another NextCloud user
Files stored in your NextCloud file directory can be selectively shared with other NextCloud users. To share a file:

 1. Locate the file you wish to share (either by navigating to the folder it is in, by searching, or by using one of the views in the sidebar).
 1. Click the file's **Share** icon (to the right of the file name)
 1. In the pane that pops out from the right, click on the search box labeled **Name, federated cloud ID or email address…**
 1. Search for the user or group you wish to share with by NextCloud user id (pospeselr), email address (richard@torproject.org), or name (Richard Pospesel) and select them from the dropdown.
 1. Optional: click on the meatball menu to the right of the shared user and edit the sharing options associated with the file or directory.
   * For instance, you may wish to automatically un-share the file at some point in the future

### Share a file with the internet
Files can also be shared with the internet via a url. Files shared in this fashion are read-only by default, but be mindful of what you share: **by default, anyone who knows the link url can download the file**. To share a file:

 1. Locate the file you wish to share
 1. Click the file's **Share** icon (to the right of the file name)
 1. In the pane that pops out from the right, click the **+** icon beside the **Share link** entry
 1. Select appropriate sharing options in the context menu (these can be changed later without invalidating the link)
 1. Optional: A few measures to limit access to a shared file:
   * Prevent general access by selecting the **Password protect** option
   * Automatically deactivate the share link at a certain time by selecting the **Set expiration date** option
 1. Finally, copy the shared link to your clipboard by clicking on the **Clipboard** icon

### Un-share files or edit their permissions
If you have shared files or folders with either the internet or another NextCloud user, you can un-share them. To un-share a file:

 1. Locate the file you wish to un-share in the **Files** app
   * All of your currently shared files and folders can be found from the **Shares** view
 1. Click the file's **Shared** icon (to the right of the file name)
 1. In the pane that pops out from the right, you get a listing of all of the users and share links associated with this file
 1. Click the meatball menu to the right of one of these listings to edit share permissions, or to delete the share entirely

### File management
### Search for a file
In the Files application press Ctrl+F, or click the magnifying glass at the upper right of the screen, and type any part of a file name.

### Desktop support
Files can be addressed transparently through [WebDAV](https://en.wikipedia.org/wiki/WebDAV). Most file explorer support the protocol which should enable you to browse the files natively on your desktop computer. Detailed instructions on how to setup various platforms are available in the [main Nextcloud documentation site about WebDAV](https://docs.nextcloud.com/server/16/user_manual/files/access_webdav.html).

But the short version is you can find the URL in the "Settings wheel" at the bottom right of the files tab, which should look something like `https://nc.torproject.net/remote.php/webdav/`. You might have to change the `https://` part to `davs://` or `webdavs://` depending on the desktop environment you are running.

If you have setup 2FA (two-factor authentication), you will also need to setup an "app password". To set that up:

 1. head to your personal settings by clicking on your icon on the top right and then `Settings`
 1. click the `Security` tab on the right
 1. in the `Devices & sessions` section, fill in an "app name" (for example, "Nautilus file manager on my desktop") and click `Create new app password`
 1. copy-paste the password and store it in your password manager
 1. click `done`

The password can now be used in your WebDAV configuration. If you fail to perform the above configuration, WebDAV connections will fail with an `Unauthorized` error message as long as 2FA is configured.

## Collaborative editing of a document
Press the plus button at the top of the file browser, it brings you a pull-down menu where you can pick "Document", "Spreadsheet", "Presentation". When you click one of those, it will become an editable field where you should put the name of the file you wish to create and hit enter, or the arrow.

### A few gotchas with collaborative editing
Behind the scenes, when a user opens a document for editing, the document is being copied from the NextCloud server to the document editing server. Once all editing sessions are closed, the document is being copied back to NextCloud. This behavior makes the following information important.

 * **_The document editing server copies documents from NextCloud**_, so while a document is open for editing it will differ from the version stored in NextCloud. The effect of this is that downloads from NextCloud will show a different version than the one currently being edited.

 * **_A document is stored back to NextCloud 10 seconds after all editing sessions for that document have finished.**_ This means that as long as there's a session open, active or idle, the versions will differ.  If either the document server breaks or the connection between NextCloud and the document server breaks it is possible that there'll be data loss.

 * **_An idle editing session expires after 5 minutes.**_ This helps making sure the document will not hang indefinitely in the document editing server even if a user leaves a browser tab open.

## Client software for both desktop (Window, macOS,Linux) and handheld (Android and iPhone)
TODO https://nextcloud.com/clients/

## Using calendars for appointments and tasks
TODO

  the prescribed way is to use the DAVx app ( https://play.google.com/store/apps/details?id=at.bitfire.davdroid&referrer=utm_source%3Dhomepage           )

NOTE: DAVx^5^ is cost free in [F-Droid](https://f-droid.org/) ([direct link](https://f-droid.org/en/packages/at.bitfire.davdroid/)).

### Importing a calendar from storm
To export a calendar from storm, click on the up/down arrows icon on the left side of the storm calendar. A menu will pop up which will allow you to "Export" a calendar in .ics format by clicking on the "Download the contents" link. This will download a file in .ics format.

The .ics file can be imported into the Next Cloud calendar app by:

 1. Clicking on the calendar icon in the top navigation bar to open up the calendar.
 1. Clicking on "Settings and Import" on the bottom left corner
 1. Click "Import Calendar" on the menu which pops up
 1. This will allow you to select the file you just downloaded from storm, ready to be uploaded
 1. You can import this calendar as a "New Calendar" or an existing calendar by expanding the drop down menu.
 1. If you select "New Calendar" you will see a new calendar, with the same name as the file you imported, appear on the left hand menu.
 1. To rename this calendar, simply click on ellipsis icon next to the calendar name in order to open up a menu with the option to edit it.

### Importing a calendar feed from Google
 1. In your Google calendar go to the "Settings and Sharing" menu (menu appears by hovering over the right hand side of your calendar's name - "Options for " and the calendar name)  for the calendar feed you want to import.
 1. Scroll down to the "Integrate Calendar" section and copy the "Secret address in iCal format" value.
 1. In Nextcloud, click on "New Subscription" and paste in the calendar link you copied above.

## Managing contacts
TODO

## Moving a spreadsheet from Sandstorm to NextCloud
 1. Download the spreadsheet document from Sandstorm by clicking the floppy disc icon that is above Row 1 and to the left of Column A.
 1. Save the file to your computer. If your spreadsheet has multiple sheets, save in Excel format.
 1. Use the instructions to [[#Uploadafile | Upload a file]] to upload the file to NextCloud.
