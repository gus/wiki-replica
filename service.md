[[_TOC_]]

# Service documentation

This documentation covers all services hosted at TPO.

Every service hosted at TPO should have a documentation page, either
in this wiki, or elsewhere (but linked here). Services should ideally
follow this [template](howto/template) to ensure proper documentation.

## Internal services

Those are services managed by TPA directly.

| Service              | Purpose                           | URL                                  | Maintainers          | Documented |
|----------------------|-----------------------------------|--------------------------------------|----------------------|------------|
| [backup][]           | Backups                           | N/A                                  | TPA                  | 75%        |
| [cache][]            | Web caching/accelerator/CDN       | N/A                                  | TPA                  | 90%        |
| [dns][]              | domain name service               | N/A                                  | TPA                  | 10%        |
| [drbd][]             | disk redundancy                   | N/A                                  | TPA                  | 10%        |
| email                | forward @torproject.org emails    | N/A                                  | TPA                  | 0%         |
| [ganeti][]           | virtual machine hosting           | N/A                                  | TPA                  | 90%        |
| [grafana][]          | metrics dashboard, pretty graphs  | <https://grafana.torproject.org>     | TPA, anarcat, hiro   | 10%        |
| [ipsec][]            | VPN                               | N/A                                  | TPA                  | 30%        |
| [kvm][]              | virtual machine hosting           | N/A                                  | TPA, weasel, anarcat | 20%        |
| [ldap][]             | host and user directory           | <https://db.torproject.org>          | TPA                  | 70%        |
| [logging][]          | centralized logging               | N/A                                  | TPA                  | 10%        |
| [nagios][]           | alerting                          | <https://nagios.torproject.org>      | TPA                  | 5%         |
| [openstack][]        | virtual machine hosting           | N/A                                  | TPA                  | 30%        |
| [postgresql][]       | database service                  | N/A                                  | TPA                  | 80%        |
| [prometheus][]       | metrics collection and monitoring | <https://prometheus.torproject.org/> | TPA, anarcat         | 90%        |
| [puppet][]           | configuration management          | `puppet.torproject.org`              | TPA                  | 100%       |
| [static-component][] | static site mirroring             | N/A                                  | TPA                  | 5%         |
| [tls][]              | X509 certificate management       | N/A                                  | TPA                  | 50%        |
| [wkd][]              | OpenPGP certificates distribution | N/A                                  | TPA                  | 10%        |

It is estimated that, on average, 42% of the documentation above is
complete. This does not include undocumented services, below.

[wkd]: howto/Web-Key-Directory-[wkd]---How-keys-are-managed-in-Tor
[tls]: howto/tls
[static-component]: howto/static-component
[puppet]: howto/puppet
[prometheus]: howto/prometheus
[postgresql]: howto/postgresql
[openstack]: howto/openstack
[nagios]: howto/nagios
[logging]: howto/logging
[ldap]: howto/ldap
[kvm]: howto/kvm
[ipsec]: howto/ipsec
[grafana]: howto/grafana
[ganeti]: howto/ganeti
[drbd]: howto/drbd
[dns]: howto/dns
[cache]: howto/cache
[backup]: howto/backup

## Non-TPA services

The following table lists services run on torproject
infrastructure. Corresponding onion services are listed on
<https://onion.torproject.org/>.

Service admins are part of tor project sys admins team. For a rough
description of what sys admin and services admin do, please have a
look [here](policy/tpa-rfc-2-support#service-admins).

The Service Admins maintain the following list of Tor Services. 

| Service            | Purpose                                                   | URL                                 | Maintainers                            | Documented |
|--------------------|-----------------------------------------------------------|-------------------------------------|----------------------------------------|------------|
| [blog][]           | web log site                                              | <https://blog.torproject.org/>      | hiro, anarcat                          | 1%         |
| [check][]          | web app to check if we're using tor                       | <https://check.torproject.org>      | arlolra                                | 90%        |
| [collector][]      | collects Tor network data and makes it available          | collector{1,2}.torproject.org       | karsten, irl                           | ?          |
| [debian archive][] | Debian package repository                                 | <https://deb.torproject.org>        | weasel                                 | 20%        |
| [git][]            | Source control system                                     | <https://git.torproject.org>        | ahf, hiro, irl, nickm, Sebastian, TPA? | 70%        |
| [gitlab][]         | Issue tracking, Wikis                                     | <https://gitlab.torproject.org/>    | ahf, hiro                              | 90%        |
| [irc][]            | IRC bouncer                                               | <ircbouncer.torproject.org>         | pastly                                 | 90%        |
| [lists][]          | mailing lists                                             | <https://lists.torproject.org>      | atagar, qbi                            | 20%        |
| [metrics][]        | network descriptor aggregator and network data visualizer | <https://metrics.torproject.org>    | karsten                                | ?          |
| [nextcloud][]      | NextCloud                                                 | <https://nc.torproject.net/>        | anarcat, gaba, hiro, ln5               | 30%        |
| [newsletter][]     | Tor Newsletter                                            | <https://newsletter.torproject.org> | ?                                      | ?          |
| [onionperf][]      | Tor network performance measurements                      | ?                                   | hiro, irl                              | ?          |
| [ooni][]           | open observatory of network interference                  | <https://ooni.torproject.org>       | hellais                                | ?          |
| [schleuder][]      | encrypted mailing lists                                   |                                     | dgoulet, hiro                          | 30%        |
| [rt][]             | Email support                                             | <https://rt.torproject.org/>        | gus, pili                              | 50%        |
| [styleguide][]     | ?                                                         | <https://styleguide.torproject.org> | antonela                               | 1%         |
| [support portal][] | ?                                                         | <https://support.torproject.org>    | pili                                   | 30%        |
| [survey][]         | survey application                                        | <https://survey.torproject.org/>    | hiro                                   | 1%         |
| [svn][]            | Document storage                                          | <https://svn.torproject.org/>       | unmaintained                           | 10%        |
| [website][]        | main website                                              | <https://www.torproject.org>        | hiro                                   | ?          |

Every service listed here must have some documentation, ideally
following the [documentation template](howto/template). As a courtesy,
TPA allows teams to maintain their documentation in a single page
here. If the documentation needs to expand beyond that, it should be
moved to its own wiki, but still linked here.

There are more (undocumented) services, listed below. Of the 20
services listed above, 6 have an unknown state because the
documentation is external (marked with `?`). Of the remaining 14
services, it is estimated that 38% of the documentation is complete.

[blog]: service/blog
[check]: https://gitlab.torproject.org/tpo/metrics/team/-/wikis/Survival-Guides/Check
[collector]: https://gitlab.torproject.org/tpo/metrics/collector/-/wikis/home
[debian archive]: service/debian-archive
[git]: howto/git
[gitlab]: howto/gitlab
[irc]: howto/irc
[lists]: service/lists
[metrics]: https://gitlab.torproject.org/tpo/metrics/team/-/wikis/home
[newsletter]: https://gitlab.torproject.org/tpo/web/newsletter/-/wikis/home
[nextcloud]: service/nextcloud
[onionperf]: https://gitlab.torproject.org/tpo/metrics/onionperf/-/wikis/home
[ooni]: https://ooni.org/
[rt]: howto/rt
[schleuder]: service/schleuder
[styleguide]: service/styleguide
[support portal]: service/support
[survey]: service/survey
[svn]: howto/svn
[website]: https://gitlab.torproject.org/tpo/web/tpo/-/wikis/home

## Undocumented service list

WARNING: this is an import of an old Trac wiki page, and no
documentation was found for those services. Ideally, each one of those
services should have a documentation page, either here or in their
team's wiki.

| Service          | Purpose                                                                       | URL                                        | Maintainers       |
|------------------|-------------------------------------------------------------------------------|--------------------------------------------|-------------------|
| archive          | package archive                                                               | <https://archive.torproject.org/>          | [boklm][]         |
| bridges          | web application and email responder to learn bridge addresses                 | <https://bridges.torproject.org/>          | phw, cohosh       |
| community        | Community Portal                                                              | <https://community.torproject.org>         | Gus               |
| consensus-health | periodically checks the Tor network for consensus conflicts and other hiccups | <https://consensus-health.torproject.org>  | tom               |
| CRM (CiviCRM)    | Donation management                                                           |                                            | GiantRabbit       |
| dist             | packages                                                                      | <https://dist.torproject.org>              | arma              |
| DocTor           | DirAuth health checks for the [tor-consensus-health@ list][]                  | <https://gitweb.torproject.org/doctor.git> | GeKo              |
| exonerator       | website that tells you whether a given IP address was a Tor relay             | <https://exonerator.torproject.org/>       | karsten           |
| extra            | static web stuff referenced from the blog (create trac ticket for access)     | <https://extra.torproject.org>             | tpa               |
| fpcentral.tbb    | Website to analyze browser fingerprint                                        | <https://fpcentral.tbb.torproject.org/>    | boklm             |
| gettor           | email responder handing out packages                                          | <https://gettor.torproject.org>            | hiro, phw, cohosh |
| jenkins          | continuous integration, autobuilding                                          | <https://jenkins.torproject.org>           | weasel            |
| media            | ?                                                                             | <https://media.torproject.org>             | hiro              |
| metricsbot       | Tor Network Status Bot (IRC, Twitter, Mastodon)                               |                                            | irl               |
| onion            | list of onion services run by the Tor project                                 | <https://onion.torproject.org>             | weasel            |
| onionoo          | web-based protocol to learn about currently running Tor relays and bridges    |                                            | karsten, irl      |
| people           | content provided by Tor people                                                | <https://people.torproject.org>            | tpa               |
| research         | website with stuff for researchers including tech reports                     | <https://research.torproject.org>          | karsten           |
| rpm archive      | RPM package repository                                                        | <https://rpm.torproject.org>               | kushal            |
| stem             | stem project website and tutorial                                             | <https://stem.torproject.org/>             | atagar            |
| tb-manual        | Tor Browser User Manual                                                       | <https://tb-manual.torproject.org/>        | gus               |
| testnet          | Test network services                                                         | ?                                          | dgoulet           |
| translation      | Translation services                                                          |                                            | emmapeel          |

[boklm]: https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/18629
[tor-consensus-health@ list]: https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-consensus-health

## Research

Those services have not been implemented yet but are at the research
phase.

| Service        | Purpose           | URL | Maintainers |
|----------------|-------------------|-----|-------------|
| [conference][] | videoconferencing | N/A | N/A         |
| [submission][] | email submission  | N/A | anarcat     |

[submission]: howto/submission
[conference]: howto/conference

## Retired

Those services have been retired.

| Service       | Purpose                       | URL                               | Maintainers         | Fate                          |
|---------------|-------------------------------|-----------------------------------|---------------------|-------------------------------|
| Atlas         | Tor relay discover            | <https://atlas.torproject.org>    | irl                 | Replaced by metrics.tpo       |
| Compass       | AS/country network diversity  | <https://compass.torproject.org>  | karsten             | ?                             |
| Globe         |                               | <https://globe.torproject.org>    |                     | Replaced by [Atlas][]         |
| Help.tpo      | TPA docs and support helpdesk | <https://help.torproject.org>     | tpa                 | Replaced by this GitLab wiki  |
| oniongit      | test GitLab instance          | <https://oniongit.eu>             | hiro                | Eventually migrated to GitLab |
| pipeline      | ?                             | <https://pipeline.torproject.org> | ?                   |                               |
| [Prodromus][] | Web chat for support team     | <https://support.torproject.org>  | phoul, lunar, helix | ?                             |
| [Trac][]      | Issues, wiki                  | <https://trac.torproject.org>     | hiro                | Migrated to GitLab, archived  |
| XMPP          | Chat/messaging                |                                   | dgoulet             | Abandoned for lack of users   |

[Trac]: howto/trac
[Prodromus]: https://github.com/rkallensee/prodromus
[Atlas]: https://atlas.torproject.org

## Documentation assesment

 * Internal: 20 services, 42% complete
 * External: 20 services, 14 documented, of which 38% are complete
   complete, 6 unknown
 * Undocumented: 23 services
 * Total: 20% of the documentation completed as of 2020-09-30
