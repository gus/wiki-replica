# Policies

The policies below document major architectural decisions taken in the
history of the team. This process is similar to the [Network Team Meta
Policy][]. More details of the process is available in the first
policy, [tpa-rfc-1-policy](policy/tpa-rfc-1-policy).

[Network Team Meta Policy]: https://gitlab.torproject.org/legacy/trac/-/wikis/org/teams/NetworkTeam/MetaPolicy

 * [tpa-rfc-1-policy](policy/tpa-rfc-1-policy)
 * [tpa-rfc-2-support](policy/tpa-rfc-2-support)
 * [tpa-rfc-3-tools](policy/tpa-rfc-3-tools)
 * [tpa-rfc-4-prometheus-disk](policy/tpa-rfc-4-prometheus-disk)
 * [tpa-rfc-5-gitlab](policy/tpa-rfc-5-gitlab)

To add a new policy, create the page using the [template](policy/template)
and add it to the above list.

