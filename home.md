# Torproject Sysadmin Team

The Torproject System Administration Team is the team that keeps
torproject.org's infrastructure going. This is the internal team wiki.
It has mostly documentation mainly targeted for the team members, but
may also have useful information for people with torproject.org
accounts.

The documentation is split into the following sections:

 * [User documentation](doc) - aimed primarily at non-technical users
   and the general public
 * [How to](howto) - procedures specifically written for sysadmins
 * [Services](service) - service documentation
 * [Policies](policy) - major decisions and how they are made
 * [Meetings](meeting) - minutes from our formal meetings

<!-- when this section is updated, _sidebar.md must be as well. -->

To contact us, see, [how to get help!][]

[how to get help!]: policy/tpa-rfc-2-support#how-to-get-help

For a list of services and which servers they run on check
[ud-ldap](https://db.torproject.org/machines.cgi).

For our source code, look at [gitweb](https://gitweb.torproject.org/)
for things under `admin/`.

This is a wiki. Feel free to send us patches to improve this resource.
You can either edit it in gitlab directly, if you can, or you can
clone ``git@gitlab.torproject.org:tpo/tpa/team.wiki`` and [send us a
patch by email][how to get help!].
